package model;

import item.Item;
import item.ItemInstance;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;

import org.epiclouds.MongoDB.MongoDAO;
import org.rosuda.REngine.Rserve.RConnection;

import predictor.Settings;

public class KmeansARIMA {
	private RConnection rConnection;
	private Kmeans kmeans;
	private ARIMA arima;

	public KmeansARIMA(RConnection rConnection) {
		this.rConnection = rConnection;
		if (checkModelUpdatingStatus()) {// 如果模型需要更新，则更新模型
			updateModel();
		} else {// 如果模型不需要更新，则从磁盘中载入模型
			loadModel();
		}
	}

	// 对一个item的数据进行预测
	public void predict(Item item) {
		// 首先，找到它属于的cluster的ID
		int clusterID = kmeans.findNearestCentroid(item);
		// 然后，用对应的ARIMA模型去预测它的销量
		arima.predict(Settings.ARIMA_MODEL_PREFIX + clusterID, item);
	}

	// 检查模型是否到需要更新的时间了
	private boolean checkModelUpdatingStatus() {
		boolean modelNeedsUpdating = false;

		File file = new File("updateTime.dat");
		if (!file.exists()) {
			modelNeedsUpdating = true;
		} else {
			long currentTime = getCurrentTime();
			long modelUpdatingTime = getModelUpdatingTime();

			int intervalDays = (int) ((currentTime - modelUpdatingTime) * 1.0 / 1000 / 3600 / 24);
			if (intervalDays >= Settings.MODEL_UPDATING_INTERVAL) {
				modelNeedsUpdating = true;
			}
		}

		return modelNeedsUpdating;
	}

	// 载入模型
	private void loadModel() {
		kmeans = new Kmeans();
		kmeans.loadModel();

		arima = new ARIMA(this.rConnection);
		arima.loadModel();
	}

	// 更新模型
	private void updateModel() {
		// 首先，获取更新模型需要的数据
		LinkedList<Item> itemList = MongoDAO.populateDataForModel();

		// 更新Kmeans模型
		kmeans = new Kmeans();
		kmeans.startClustering(itemList);
		kmeans.saveModel();

		// 更新ARIMA模型，每个Kmeans的cluster都对应一个ARIMA模型
		ArrayList<ItemInstance>[] clusters = kmeans.getClusters();
		arima = new ARIMA(this.rConnection);
		// System.out.println("cluster sizes and centroids: ");
		for (int clusterID = 0; clusterID < clusters.length; clusterID++) {
			// System.out.println("[" + i + "]" + " " + clusters[i].size());
			// System.out.println(kmeans.getCentroid(i));
			String modelName = Settings.ARIMA_MODEL_PREFIX + clusterID;
			arima.fitRModel(modelName, clusters[clusterID]);
		}
		arima.saveModel();

		// 存储模型的更新时间
		long currentTime = getCurrentTime();
		saveTime(currentTime);
	}

	// 获取当前时间
	private long getCurrentTime() {
		Calendar currentCal = Calendar.getInstance();
		return currentCal.getTimeInMillis();
	}

	// 存储时间到磁盘
	private void saveTime(long time) {
		try {
			@SuppressWarnings("resource")
			ObjectOutputStream output = new ObjectOutputStream(
					new FileOutputStream("updateTime.dat"));
			output.writeObject(new Long(time));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// 获取上次模型的更新时间
	private long getModelUpdatingTime() {
		long time = 0;
		try {
			@SuppressWarnings("resource")
			ObjectInputStream input = new ObjectInputStream(
					new FileInputStream("updateTime.dat"));
			Long timeLong = (Long) input.readObject();
			time = (long) timeLong;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		return time;
	}
}
