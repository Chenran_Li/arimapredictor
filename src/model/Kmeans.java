package model;

import item.Item;
import item.ItemInstance;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

import predictor.Settings;
import weka.clusterers.SimpleKMeans;
import weka.core.Attribute;
import weka.core.EuclideanDistance;
import weka.core.FastVector;
import weka.core.Instances;

public class Kmeans {
	private int numClusters;// cluster数量
	private int numIterations;// Kmeans循环次数
	private int dimensions;// 维度数，这里为2
	private double alpha;// 平均销量的系数
	private double beta;// 平均价格的系数

	private ArrayList<ItemInstance> centroids = new ArrayList<ItemInstance>();// 聚类中心点
	private ArrayList<ItemInstance>[] clusters;// 聚出的类
	private FastVector fvWekaAttributes;// weka中的数据结构，可忽略
	private EuclideanDistance euclideanDistance;// 可忽略

	private SimpleKMeans simpleKMeans = new SimpleKMeans();// weka的Kmeans类

	public Kmeans() {
		this.dimensions = Settings.NUM_KMEANS_DIMENSIONS;
		this.alpha = Settings.COEFFICIENT_AVERAGE_SALES;
		this.beta = Settings.COEFFICIENT_AVERAGE_PRICE;
	}

	// 将模型存储到磁盘中
	public void saveModel() {
		try {
			System.out.println("Saving Kmeans Model ...");
			@SuppressWarnings("resource")
			ObjectOutputStream output = new ObjectOutputStream(
					new FileOutputStream("Kmeans.dat"));
			output.writeObject(centroids);
			output.writeObject(euclideanDistance);
			output.writeObject(fvWekaAttributes);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// 从磁盘中读取模型
	@SuppressWarnings("unchecked")
	public void loadModel() {
		try {
			System.out.println("Loading Kmeans Model ...");
			@SuppressWarnings("resource")
			ObjectInputStream input = new ObjectInputStream(
					new FileInputStream("Kmeans.dat"));
			centroids = (ArrayList<ItemInstance>) input.readObject();
			euclideanDistance = (EuclideanDistance) input.readObject();
			fvWekaAttributes = (FastVector) input.readObject();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	// 开始运行Kmeans
	@SuppressWarnings("unchecked")
	public void startClustering(LinkedList<Item> itemList) {
		this.numClusters = findingK(itemList.size());
		this.numIterations = Settings.NUM_KMEANS_ITERATIONS;

		Instances dataToBeClustered = generateARIMAKmeansData(itemList);
		try {
			this.simpleKMeans.setMaxIterations(this.numIterations);
			this.simpleKMeans.setNumClusters(this.numClusters);
			this.simpleKMeans.setPreserveInstancesOrder(true);
			this.simpleKMeans.setSeed(2);
			this.simpleKMeans.buildClusterer(dataToBeClustered);
			this.numClusters = this.simpleKMeans.getNumClusters();
			this.euclideanDistance = (EuclideanDistance) this.simpleKMeans
					.getDistanceFunction();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// 得到cluster
		this.clusters = new ArrayList[this.numClusters];
		for (int i = 0; i < this.numClusters; i++) {
			this.clusters[i] = new ArrayList<ItemInstance>();
		}

		int[] assignments = null;
		try {
			assignments = simpleKMeans.getAssignments();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Iterator<Item> itr = itemList.iterator();
		int trainingDataCounter = 0;
		while (itr.hasNext()) {
			Item item = itr.next();
			ItemInstance itemInstance = generateItemInstance(item);
			this.clusters[assignments[trainingDataCounter]].add(itemInstance);
			trainingDataCounter++;
		}

		// 得到聚类中心点
		Instances instances = this.simpleKMeans.getClusterCentroids();
		for (int i = 0; i < this.numClusters; i++) {
			ItemInstance itemInstance = new ItemInstance(instances.instance(i));
			this.centroids.add(itemInstance);
		}
	}

	// 将item的数据转换成Kmeans支持的数据格式
	private Instances generateARIMAKmeansData(LinkedList<Item> itemList) {
		this.fvWekaAttributes = new FastVector(this.dimensions);
		for (int i = 0; i < this.dimensions; i++) {
			this.fvWekaAttributes.addElement(new Attribute("Dim No. " + i));
		}
		Instances dataToBeClustered = new Instances("Rel",
				this.fvWekaAttributes, itemList.size());
		Iterator<Item> itr = itemList.iterator();
		while (itr.hasNext()) {
			Item thisItem = itr.next();
			ItemInstance thisItemInstance = generateItemInstance(thisItem);
			dataToBeClustered.add(thisItemInstance);
		}

		return dataToBeClustered;
	}

	// 将item转换成itemInstance
	private ItemInstance generateItemInstance(Item item) {
		ItemInstance itemInstance = new ItemInstance(this.dimensions, item);
		itemInstance.setValue((Attribute) this.fvWekaAttributes.elementAt(0),
				this.alpha * item.getAverageSalesWeekly());
		itemInstance.setValue((Attribute) this.fvWekaAttributes.elementAt(1),
				this.beta * item.getAveragePrice());

		return itemInstance;
	}

	// 对于一个item，找到它属于的类
	public int findNearestCentroid(Item item) {
		ItemInstance itemInstance = generateItemInstance(item);
		int nearestCentroid = 0;
		double smallestDistance = 1e10;
		Iterator<ItemInstance> itr = this.centroids.iterator();
		int currentCentroid = 0;
		while (itr.hasNext()) {
			double distance = 0;
			distance = euclideanDistance.distance(itemInstance, itr.next());
			if (distance < smallestDistance) {
				nearestCentroid = currentCentroid;
				smallestDistance = distance;
			}
			currentCentroid++;
		}
		return nearestCentroid;
	}

	public ArrayList<ItemInstance>[] getClusters() {
		return this.clusters;
	}

	public ItemInstance getCentroid(int centroidID) {
		return this.centroids.get(centroidID);
	}

	public void removeCentroid(int centroidID) {
		if (centroidID < this.centroids.size()) {
			this.centroids.remove(centroidID);
			this.numClusters--;
		}
	}

	// 得到Kmeans中K的值
	public static int findingK(int dataSize) {
		int k = (int) Math.sqrt(dataSize / 2);
		System.out.println("cluster number: " + k);
		return k;
	}
}
