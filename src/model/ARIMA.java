package model;

import item.Item;
import item.ItemInstance;

import java.util.ArrayList;
import java.util.TreeMap;

import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

import predictor.Settings;

public class ARIMA {

	private RConnection rConnection;

	public ARIMA(RConnection rConnection) {
		this.rConnection = rConnection;
	}

	// 从磁盘中载入模型数据
	public void loadModel() {
		try {
			rConnection.eval("library(forecast)");
			System.out.println("Loading ARIMA Model ...");
			rConnection.eval("load(" + "\"" + System.getProperty("user.dir")
					+ System.getProperty("file.separator") + "ARIMA.RData"
					+ "\"" + ")");
		} catch (RserveException e) {
			e.printStackTrace();
		}
	}

	// 将模型数据存储到磁盘
	public void saveModel() {
		try {
			System.out.println("Saving ARIMA Model ...");
			rConnection.eval("save.image(" + "\""
					+ System.getProperty("user.dir")
					+ System.getProperty("file.separator") + "ARIMA.RData"
					+ "\"" + ")");
		} catch (RserveException e) {
			e.printStackTrace();
		}
	}

	// 将销量数据输入到ARIMA模型
	public void fitRModel(String modelName, ArrayList<ItemInstance> cluster) {
		double[] wholeDataSeries = generateDataSeries(cluster);
		try {
			rConnection.eval("library(forecast)");
			rConnection.assign("sales", wholeDataSeries);
			rConnection.eval("sales <- as.numeric(sales);"
					+ "sales <- ts(sales);" + modelName
					+ " <- auto.arima(sales);");
		} catch (REngineException e) {
			e.printStackTrace();
		}
	}

	// 预测某一天的销量
	private double predictOneDaySales(int[] historyTrueValues, String modelName) {
		try {
			rConnection.assign("x", historyTrueValues);
			rConnection.eval("refit <- Arima(x, model=" + modelName + ");"
					+ "forecastNumber = forecast(refit,h=" + "1" + ")$mean;");
			return rConnection.eval("forecastNumber").asDoubles()[0];
		} catch (RserveException e) {
			e.printStackTrace();
		} catch (REXPMismatchException e) {
			e.printStackTrace();
		} catch (REngineException e) {
			e.printStackTrace();
		}

		return 0;
	}

	// 预测若干天的销量
	private double[] predictSales(int[] historyTrueValues, int predictLength,
			String modelName) {
		try {
			rConnection.assign("x", historyTrueValues);
			rConnection.eval("refit <- Arima(x, model=" + modelName + ");"
					+ "forecastNumber = forecast(refit,h=" + predictLength
					+ ")$mean;");
			return rConnection.eval("forecastNumber").asDoubles();
		} catch (RserveException e) {
			e.printStackTrace();
		} catch (REXPMismatchException e) {
			e.printStackTrace();
		} catch (REngineException e) {
			e.printStackTrace();
		}

		return null;
	}

	// 预测某一个item的未来销量，每一个Kmeans的cluster都对应一个ARIMA模型，这里的modelName代表了它需要用到哪一个ARIMA模型
	// 这是由Kmeans的结果决定的，比如这个item被聚到了第0类，则modelName=arima_model0
	public void predict(String modelName, Item item) {
		int numWeeks = Settings.DISPLAY_LENGTH + Settings.PREDICTION_LENGTH;
		double[] results = new double[numWeeks];
		for (int weekCounter = 0; weekCounter < Settings.DISPLAY_LENGTH; weekCounter++) {
			int[] historySales = item.getHistorySales(0, weekCounter
					+ Settings.ARIMA_HISTORY_LENGTH);
			results[weekCounter] = predictOneDaySales(historySales, modelName);
		}
		double[] futureSales = predictSales(
				item.getHistorySales(0, Settings.DISPLAY_LENGTH
						+ Settings.ARIMA_HISTORY_LENGTH),
				Settings.PREDICTION_LENGTH, modelName);
		for (int futureWeekCounter = 0; futureWeekCounter < Settings.PREDICTION_LENGTH; futureWeekCounter++) {
			results[Settings.DISPLAY_LENGTH + futureWeekCounter] = futureSales[futureWeekCounter];
		}
		item.setPredictedSales(results);
	}

	// 根据这个cluster里面所有item的销售历史值，生成一个序列，以输入到ARIMA模型中
	private double[] generateDataSeries(ArrayList<ItemInstance> cluster) {
		ArrayList<Double> doubleList = new ArrayList<Double>();

		for (int itemCounter = 0; itemCounter < cluster.size(); itemCounter++) {
			Item item = cluster.get(itemCounter).getItem();
			TreeMap<Integer, Integer> itemSoldWeekly = item.getItemSoldWeekly();
			for (int j = itemSoldWeekly.firstKey(); j <= itemSoldWeekly
					.lastKey(); j++) {
				doubleList.add(new Double(itemSoldWeekly.get(j)));
			}
			doubleList.add(Double.NaN);
			doubleList.add(Double.NaN);
			doubleList.add(Double.NaN);
		}

		double[] dataSeries = new double[doubleList.size()];
		for (int i = 0; i < doubleList.size(); i++) {
			dataSeries[i] = doubleList.get(i);
		}

		return dataSeries;
	}
}
