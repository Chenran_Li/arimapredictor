package item;

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.TreeMap;

import predictor.Settings;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

//供更新模型的时候用
public class ItemForModel extends Item {

	public ItemForModel(String storeName, String storePlatform,
			String itemName, String itemID, String data, int num_recent_weeks) {
		super(storeName, storePlatform, itemName, itemID);
		parseData(data, num_recent_weeks);// 更新模型的时候，用最近num_recent_weeks周的数据
	}

	private void parseData(String data, int num_recent_weeks) {
		JSONObject obj = JSONObject.parseObject(data);
		JSONArray itemSold = obj.getJSONObject("items_sold").getJSONArray(
				"data");
		JSONArray itemPrice = obj.getJSONObject("average_end_price")
				.getJSONArray("data");

		// 获取这个item的最新销售数据的获取日期
		Date latestDate = findLatestDate(itemSold, itemPrice);
		// 设定需要的销售数据的周数
		int numWeeks = Settings.NUM_RECENT_WEEKS;
		// 设定需要的销售数据的天数
		int numDays = 7 * numWeeks;
		Calendar tempCalendar = Calendar.getInstance();
		tempCalendar.setTime(latestDate);
		// 用最新的日期减去需要的销售数据的天数，就是起始日期
		tempCalendar.add(Calendar.DATE, -numDays + 1);
		// 设定使用的销售数据的起始日期
		this.startDate = tempCalendar.getTime();

		itemSoldDaily = new TreeMap<Integer, Integer>();
		itemPriceDaily = new TreeMap<Integer, Double>();

		// 获取日销量以及每天的价格
		for (int i = 0; i < itemSold.size(); i++) {
			if (itemSold.getJSONArray(i).size() > 1
					&& (itemPrice.getJSONArray(i).size() > 1)) {
				String date = itemSold.getJSONArray(i).getString(0);
				date = date.split(" ")[0];
				String dateString[] = date.split("/");
				int intervalToStart = getDateInterval(dateString, startDate);
				int intervalToEnd = getDateInterval(dateString, latestDate);
				if (intervalToStart >= 0 && intervalToEnd <= 0) {
					itemSoldDaily.put(intervalToStart, itemSold.getJSONArray(i)
							.getInteger(1));
					itemPriceDaily.put(intervalToStart,
							itemPrice.getJSONArray(i).getDouble(1));
				}
			}
		}

		// 如果有销量的天数不足，则item的status被设置成相应的值
		if (itemSoldDaily.size() < Settings.NUM_DAYS_FEWEST) {
			this.status = VALID_DAYS_TOO_FEW;
		}

		// status为OK才进行下一步处理
		if (this.status == OK) {
			// 计算平均价格
			Iterator<Integer> itr = itemPriceDaily.keySet().iterator();
			int numValidDays = 0;
			double totalPrice = 0;
			while (itr.hasNext()) {
				double price = itemPriceDaily.get(itr.next());
				totalPrice += price;
				numValidDays++;
			}
			this.averagePrice = totalPrice / numValidDays;

			// 获取每周销量
			itemSoldWeekly = new TreeMap<Integer, Integer>();
			historySales = new int[numWeeks];
			for (int weekCount = 0; weekCount < numWeeks; weekCount++) {
				int weekSales = 0;
				for (int dayCount = 0; dayCount < 7; dayCount++) {
					int realDayCount = weekCount * 7 + dayCount;
					if (itemSoldDaily.containsKey(realDayCount)) {
						weekSales += itemSoldDaily.get(realDayCount);
					}
				}
				itemSoldWeekly.put(weekCount, weekSales);
				historySales[weekCount] = weekSales;
			}

			// 获取周平均销量
			double totalSales = 0;
			for (int weekCount = 0; weekCount < numWeeks; weekCount++) {
				totalSales += itemSoldWeekly.get(weekCount);
			}
			this.averageSalesWeekly = totalSales / numWeeks;

			// 如果平均销量不达标，则status也会被置为相应的值
			if (this.averageSalesWeekly < Settings.MODEL_WEEKLY_AVERAGE_SALES_THRESHOLD) {
				this.status = AVERAGE_SALES_TOO_LOW;
			}
		}
	}
}
