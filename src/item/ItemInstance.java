package item;

import java.io.Serializable;

import weka.core.Instance;

public class ItemInstance extends Instance implements Serializable {
	private static final long serialVersionUID = 4326353658516078830L;
	private transient Item item;

	public ItemInstance(int dimensions, Item item) {
		super(dimensions);
		this.item = item;
	}

	public ItemInstance(Instance instance) {
		super(instance);
	}

	public Item getItem() {
		return item;
	}
}
