package item;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TreeMap;

import org.epiclouds.bean.ChartBean;
import org.epiclouds.bean.Measure;
import org.epiclouds.mySQL.bean.StoreItemBean;

import predictor.Settings;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public abstract class Item {
	// 以下是跟后端数据库对应的一些字段
	private String storeName;
	private String storePlatform;
	private String itemID;
	private String itemName;
	protected double itemPriceLower;
	protected double itemPriceUpper;
	protected int itemSalesLast90Days;
	private int itemSalesFuture4Weeks;
	protected double predictAccuracy;
	private ChartBean chartBean;

	// 记录这个item的数据需要从哪一天开始展示
	protected Date startDate;

	// item的日销量
	protected TreeMap<Integer, Integer> itemSoldDaily = new TreeMap<Integer, Integer>();
	// item每天的价格
	protected TreeMap<Integer, Double> itemPriceDaily = new TreeMap<Integer, Double>();
	// item的周销量
	protected TreeMap<Integer, Integer> itemSoldWeekly;

	public static final int OK = 0;// 符合要求
	public static final int VALID_DAYS_TOO_FEW = 1;// 有销量的天数过少
	public static final int AVERAGE_SALES_TOO_LOW = 2;// 平均销量过低
	public static final int HISTORY_NOT_LONG_ENOUGH = 3;// 历史天数不足以运行ARIMA

	// 负责记录这个item的数据的状态，如果最后的状态不是OK，这个item最后会被删除掉，不会被插入到MySQL中
	protected int status = OK;
	// 平均价格
	protected double averagePrice;
	// 周平均销量
	protected double averageSalesWeekly;
	// 这个item的历史若干周的销量
	protected int[] historySales;

	// protected，不能直接初始化此类，只能初始化两个子类
	protected Item(String storeName, String storePlatform, String itemName,
			String itemID) {
		this.storeName = storeName;
		this.storePlatform = storePlatform;
		this.itemName = itemName;
		this.itemID = itemID;
	}

	// 由此item类转换成可以直接插入MySQL的StoreItemBean
	public StoreItemBean generateStoreItemBean() {
		// 转换成百分比形式
		String predictAccuracyString = Double.toString(predictAccuracy * 100);
		// 由于MySQL中只能存放两位小数，因此这里砍掉后面的位数
		if (predictAccuracyString.length() > 5) {
			predictAccuracyString = predictAccuracyString.substring(0,
					predictAccuracyString.indexOf('.') + 3);
		}
		StoreItemBean storeItemBean = new StoreItemBean(storeName,
				storePlatform, itemID, itemName, Settings.ITEM_URL_PREFIX
						+ itemID, "", "", itemPriceLower, itemPriceUpper,
				itemSalesLast90Days, itemSalesFuture4Weeks,
				predictAccuracyString, JSONObject.toJSONString(chartBean));
		return storeItemBean;
	}

	// 根据价格的预测值生成ChartBean，即前端需要的图表信息
	public void setPredictedSales(double[] predictedSales) {
		// 得到周数（即需要展示的历史周数+预测周数）
		int numWeeks = Settings.DISPLAY_LENGTH + Settings.PREDICTION_LENGTH;
		int[] predictedSalesInt = new int[numWeeks];
		// 获取每天预测数据的整数值（因为前端要显示整数）
		for (int i = 0; i < numWeeks; i++) {
			predictedSalesInt[i] = new BigDecimal(predictedSales[i]).setScale(
					0, BigDecimal.ROUND_HALF_UP).intValue();
		}

		// 获取未来四周的预测销量
		itemSalesFuture4Weeks = 0;
		for (int i = 0; i < Settings.PREDICTION_LENGTH; i++) {
			itemSalesFuture4Weeks += predictedSalesInt[Settings.DISPLAY_LENGTH
					+ i];
		}

		// 获取预测准确度的值
		double predictError = 0;
		double totalTrueValue = 0;
		int[] sales = getHistorySales(Settings.ARIMA_HISTORY_LENGTH,
				Settings.ARIMA_HISTORY_LENGTH + Settings.DISPLAY_LENGTH);
		for (int i = 0; i < Settings.DISPLAY_LENGTH; i++) {
			totalTrueValue += sales[i];
			predictError += Math.abs(sales[i] - predictedSalesInt[i]);
		}
		this.predictAccuracy = 1 - predictError / totalTrueValue;

		// 生成MeasureBean，即价格走势
		Measure measure = new Measure(sales, predictedSalesInt);

		// 输出结果
		// System.out.print("real sales: ");
		// for (int i = 0; i < sales.length; i++) {
		// System.out.print(sales[i] + " ");
		// }
		// System.out.println();
		//
		// System.out.print("predicted sales: ");
		// for (int i = 0; i < predictedSalesInt.length; i++) {
		// System.out.print(predictedSalesInt[i] + " ");
		// }
		// System.out.println();
		// System.out.println();

		// 由于chartBean里面需要诸如“3.22~3.28”之类的时间标签，以下代码的作用就是生成这些标签
		String[] dateList = new String[numWeeks];// 时间标签的list
		for (int i = 0; i < numWeeks; i++) {
			SimpleDateFormat dateFormat = new SimpleDateFormat("M.d");
			Calendar tempCalendar = Calendar.getInstance();
			tempCalendar.setTime(this.startDate);
			tempCalendar.add(Calendar.DATE, i * 7);
			Date tempDate = tempCalendar.getTime();
			String weekDate = dateFormat.format(tempDate);
			tempCalendar.add(Calendar.DATE, 6);
			tempDate = tempCalendar.getTime();
			weekDate += "~" + dateFormat.format(tempDate);
			dateList[i] = weekDate;
		}
		// 生成ChartBean
		this.chartBean = new ChartBean(dateList, measure);
	}

	// 找出这个item的最新数据是在哪一天（因为爬虫不是每天更新价格的，所以这个item的最新的数据可能不是今天）
	protected Date findLatestDate(JSONArray itemSold, JSONArray itemPrice) {
		int latestDateInt = 0;
		for (int i = 0; i < itemSold.size(); i++) {
			String date = itemSold.getJSONArray(i).getString(0);
			date = date.split(" ")[0];
			String dateString[] = date.split("/");
			int dateInt = Integer.parseInt(dateString[0]) * 10000
					+ Integer.parseInt(dateString[1]) * 100
					+ Integer.parseInt(dateString[2]);
			if (latestDateInt == 0) {
				latestDateInt = dateInt;
				continue;
			}
			if (dateInt > latestDateInt) {
				latestDateInt = dateInt;
			}
		}

		Calendar latestDateCal = Calendar.getInstance();
		latestDateCal.set(latestDateInt / 10000,
				latestDateInt % 10000 / 100 - 1, latestDateInt % 100, 0, 0, 0);
		Date latestDate = latestDateCal.getTime();
		return latestDate;
	}

	// 指定两个日期，获取这两个日期中间间隔的天数
	protected int getDateInterval(String[] dateString, Date date) {
		Calendar cal = Calendar.getInstance();
		cal.set(Integer.parseInt(dateString[0]),
				Integer.parseInt(dateString[1]) - 1,
				Integer.parseInt(dateString[2]), 0, 0, 0);
		return (int) ((cal.getTimeInMillis() - date.getTime()) / (1000 * 60 * 60 * 24));
	}

	public TreeMap<Integer, Integer> getItemSoldWeekly() {
		return itemSoldWeekly;
	}

	public int getStatus() {
		return status;
	}

	public double getAveragePrice() {
		return averagePrice;
	}

	public double getAverageSalesWeekly() {
		return averageSalesWeekly;
	}

	public int[] getHistorySales(int start, int end) {
		int[] sales = new int[end - start];
		for (int i = start; i < end; i++) {
			sales[i - start] = historySales[i];
		}
		return sales;
	}
}
