package predictor;

public class Settings {
	// MySQL相关
	public static final String MYSQL_URL = "jdbc:mysql://120.26.45.160:3306/yuanshujutest";
	public static final String MYSQL_USER = "root";
	public static final String MYSQL_PASSWORD = "123Yuanshuju456";
	public static final String MYSQL_DRIVER_CLASS = "com.mysql.jdbc.Driver";

	// MongoDB相关
	// public static final String MONGO_SERVER_IP = "106.3.38.50";
	public static final String MONGO_SERVER_IP = "127.0.0.1";
	public static final String MONGO_STORE_DB_NAME = "ebay";

	// 以下的参数跟预测部分有关
	public static final String PLATFORM = "ebay";// 要预测的数据属于的平台
	public static final String ITEM_URL_PREFIX = "www.ebay.com/itm/";// 这里是Item的链接前缀，前缀加上URL组成Item的链接（可能只对ebay平台的商品有效）
	public static final String[] STORES = { "elec-mall" };// 要对哪些商店的数据进行预测

	public static final int ARIMA_HISTORY_LENGTH = 4;// ARIMA模型需要至少X周的历史数据来预测
	public static final int DISPLAY_LENGTH = 12;// 前端展示最近X周的历史数据
	public static final int PREDICTION_LENGTH = 4;// 预测未来X周的数据
	public static final int NUM_DAYS_THRESHOLD = 40;// 限制商品的有效天数，有效天数过少的商品会被舍弃。有效天数即有销量的天数。
	public static final double WEEKLY_AVERAGE_SALES_THRESHOLD = 7.0;// 限制商品的周平均销量，周平均销量过少的商品会被舍弃。
	public static final double COEFFICIENT_AVERAGE_SALES = 0.1;// Kmeans中，平均销量的权重
	public static final double COEFFICIENT_AVERAGE_PRICE = 1;// Kmeans中，平均价格的权重
	public static final int NUM_KMEANS_ITERATIONS = 1000000;// Kmeans的循环次数
	public static final int NUM_KMEANS_DIMENSIONS = 2;// Kmeans数据点的维度，平均价格和平均销量两个维度，所以设置为2

	// 以下的参数跟更新模型有关
	public static final int MODEL_UPDATING_INTERVAL = 7;// 隔多少天更新一次模型
	public static final String[] MODEL_STORES = { "elec-mall" };// 在更新模型的时候，用哪些商店的数据
	public static final int NUM_RECENT_WEEKS = 15;// 在更新模型的时候，用最近X周的数据
	public static final int NUM_DAYS_FEWEST = 40;// 在更新模型的时候，限制商品的有效天数，有效天数过少的商品会被舍弃。有效天数即有销量的天数。
	public static final double MODEL_WEEKLY_AVERAGE_SALES_THRESHOLD = 7.0;// 限制商品的周平均销量，周平均销量过少的商品会被舍弃。
	public static final String ARIMA_MODEL_PREFIX = "arima_model";// 这个请忽略，是ARIMA模型的统一变量前缀
}
