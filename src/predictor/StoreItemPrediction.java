package predictor;

import item.Item;

import java.util.Iterator;
import java.util.LinkedList;

import model.KmeansARIMA;

import org.epiclouds.MongoDB.MongoDAO;
import org.epiclouds.mySQL.DatabaseDAO;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

public class StoreItemPrediction {
	private LinkedList<Item> itemList;
	private RConnection rConnection;

	StoreItemPrediction() {
		try {
			rConnection = new RConnection();
		} catch (RserveException e) {
			e.printStackTrace();
		}
	}

	// 整个工程的主要逻辑
	public void run() {
		System.out.println("Loading data from MongoDB ...");
		this.itemList = MongoDAO.populateDataForPrediction();// 从MongoDB中获取要预测的item的list

		System.out.println("Running prediction ...");
		runKmeansARIMA(rConnection);// 对数据进行预测

		System.out.println("Inserting data into MySQL ...\n");
		Iterator<Item> itr = this.itemList.iterator();
		while (itr.hasNext()) {// 遍历list中的每一个item
			Item thisItem = itr.next();
			DatabaseDAO.insertIntoStoreItemDB(thisItem.generateStoreItemBean());// 插入到MySQL中
		}

		System.out.println("Completed.");
	}

	// 用模型进行预测
	private void runKmeansARIMA(RConnection rConnection) {
		KmeansARIMA kmeansARIMA = new KmeansARIMA(rConnection);// 载入模型
		Iterator<Item> itr = this.itemList.iterator();
		while (itr.hasNext()) {
			kmeansARIMA.predict(itr.next());// 遍历list中的每一个item，分别进行预测
		}
	}
}
